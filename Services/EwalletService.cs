﻿using Microsoft.EntityFrameworkCore;
using Quis1_William.Entities;
using Quis1_William.Models;
using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;

namespace Quis1_William.Services
{
    public class EwalletService
    {
        private readonly ManagementDbContext _db;

        public EwalletService(ManagementDbContext dbContext)
        {
            this._db = dbContext;
        }

        /// <summary>
        /// function untuk ambil semua data 
        /// </summary>
        /// <returns></returns>
        public async Task<List<EWalletModel>> GetAllEwalletAsync()
        {
            var ewallets = await this._db.
                Ewallets
                .Select(Q => new EWalletModel
                {
                    NamaEwallet = Q.NamaEwallet,
                    IdEwallet = Q.IdEwallet,
                    JumlahEwallet = Q.JumlahEwallet
                })
                .AsNoTracking()
                .ToListAsync();

            return ewallets;
        }

        /// <summary>
        /// function untuk insert data
        /// </summary>
        /// <param name="eWalletModel"></param>
        /// <returns></returns>

        public async Task<bool> InsertEwalletAsync(int id, string name, int jumlah)
        {
            this._db.Add(new Ewallet
            {
                IdEwallet = id,
                NamaEwallet = name,
                JumlahEwallet = jumlah
            });

            await this._db.SaveChangesAsync();
            return true;
        }

        /// <summary>
        /// function untuk getSpecific data nya berdasarkan id
        /// </summary>
        /// <param name="Idwallet"></param>
        /// <returns></returns>

        public async Task<EWalletModel> GetSpecificEwalleyAsync(int? Idwallet)
        {
            var ewallet = await this._db
                .Ewallets
                .Where(Q => Q.IdEwallet == Idwallet)
                .Select(Q => new EWalletModel
                {
                    IdEwallet = Q.IdEwallet,
                    NamaEwallet = Q.NamaEwallet,
                    JumlahEwallet = Q.JumlahEwallet
                })
                .FirstOrDefaultAsync();
                return ewallet;
        }

        /// <summary>
        ///function untuk update data nya
        /// </summary>
        /// <param name="eWallet"></param>
        /// <returns></returns>

        public async Task<bool> UpdateEwalletAsync(EWalletModel eWallet)
        {
            var ewalletModel = await this._db.Ewallets
                .Where(Q => Q.IdEwallet == eWallet.IdEwallet)
                .FirstOrDefaultAsync();

            if (ewalletModel == null)
            {
                return false;
            }

            ewalletModel.NamaEwallet = eWallet.NamaEwallet;
            ewalletModel.JumlahEwallet = eWallet.JumlahEwallet;

            await this._db.SaveChangesAsync();
            return true;
        }

        /// <summary>
        /// function untuk delete data nya
        /// </summary>
        /// <param name="idEwallet"></param>
        /// <returns></returns>

        public async Task<bool> DeleteEwalletAsync(int idEwallet)
        {
            var ewalletModel = await this._db.Ewallets
                .Where(Q => Q.IdEwallet == idEwallet)
                .FirstOrDefaultAsync();

            if (ewalletModel == null)
            {
                return false;
            }

            this._db.Remove(ewalletModel);
            await this._db.SaveChangesAsync();

            return true;
        }

        public async Task<List<EWalletModel>> GetAsync(int pageIndex, int itemPerPage, string filterByName)
        {
            // set employee to queryable for  filter
            var query = this._db
                .Ewallets
                .AsQueryable();

            // for filter
            if (string.IsNullOrEmpty(filterByName) == false)
            {
                query = query
                    .Where(Q => Q.NamaEwallet.StartsWith(filterByName));
            }

            // get data after filtering
            var ewallets = await query
                .Select(Q => new EWalletModel
                {
                    IdEwallet = Q.IdEwallet,
                    NamaEwallet = Q.NamaEwallet,
                    JumlahEwallet = Q.JumlahEwallet
                   
                })
                .Skip((pageIndex - 1) * itemPerPage)
                .Take(itemPerPage)
                .AsNoTracking()
                .ToListAsync();

            return ewallets;
        }

        public int GetTotalData()
        {
            var totalEwallet = this._db.Ewallets
                .Count();

            return totalEwallet;
        }
    }
}
