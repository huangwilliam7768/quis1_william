﻿using System;
using System.Collections.Generic;
using System.ComponentModel.DataAnnotations;
using System.Linq;
using System.Threading.Tasks;

namespace Quis1_William.Models
{
    public class EWalletModel
    {
        /// <summary>
        /// untuk property id
        /// </summary>
        public int IdEwallet { get; set; }

        /// <summary>
        /// untuk property name
        /// </summary>
        [Required]
        [StringLength (7,MinimumLength =3)]
        public string NamaEwallet { get; set; }

        /// <summary>
        /// untuk property jumlah
        /// </summary>
        [Required]
        public int JumlahEwallet { get; set; }


    }
}
