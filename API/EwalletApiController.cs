﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Threading.Tasks;
using Microsoft.AspNetCore.Http;
using Microsoft.AspNetCore.Mvc;
using Quis1_William.Models;
using Quis1_William.Services;

namespace Quis1_William.API
{
    [Route("api/v1/ewallet")]
    [ApiController]
    public class EwalletApiController : ControllerBase
    {
        private readonly EwalletService _serviceMan;

        public EwalletApiController(EwalletService ewalletService)
        {
            this._serviceMan = ewalletService;
        }

        /// <summary>
        /// ini api untuk mengambil semua data
        /// </summary>
        /// <returns></returns>
        [HttpGet ("all-ewallet",Name ="allEwallet")]

        public async Task<ActionResult<List<EWalletModel>>>GetAllEwalletAsync()
        {
            var ewallets = await this._serviceMan.GetAllEwalletAsync();
            return Ok(ewallets);
        }

        /// <summary>
        /// ini api untuk insert data
        /// </summary>
        /// <param name="value"></param>
        /// <returns></returns>

        [HttpPost ("insert",Name ="insertEwallet")]

        public async Task<ActionResult<ResponseModel>> InsertNewEwalletAsync(int id,[FromQuery]string name,[FromQuery] int jumlah)
        {
            var isSucces = await this._serviceMan.InsertEwalletAsync(id,name,jumlah);

            if (isSucces == false)
            {
                return BadRequest(new ResponseModel
                {
                    ResponseMessage = "Id not found!!"  
                });
            }

            return Ok(new ResponseModel
            {
                ResponseMessage = "Success insert data!!"
            });
        }

        /// <summary>
        /// ini api untuk mengambil data specific berdasarkan id nya
        /// </summary>
        /// <param name="idEwallet"></param>
        /// <returns></returns>
        [HttpGet ("specific-ewallet/{idEwallet}",Name ="getSpecificEwallet")]

        public async Task<ActionResult<EWalletModel>> GetSpecificEwalletAsync(int? idEwallet)
        {
            if (idEwallet.HasValue == false)
            {
                return BadRequest(null);
            }

            var ewallet = await _serviceMan.GetSpecificEwalleyAsync(idEwallet.Value);
            if (ewallet == null)
            {
                return BadRequest(null);
            }
            return Ok(ewallet);
        }

        [HttpPut ("update",Name ="updateEwallet")]

        public async Task<ActionResult<ResponseModel>> UpdateEwalletAsync([FromBody] EWalletModel value)
        {
            var isSuccess = await this._serviceMan.UpdateEwalletAsync(value);

            if (isSuccess == false)
            {
                return BadRequest(new ResponseModel
                {
                    ResponseMessage = "Id not found"
                });
            }
            return Ok(new ResponseModel
            {
                ResponseMessage = "Success Update data"
            });
        }

        [HttpDelete ("delete",Name ="deleteEwallet")]

        public async Task<ActionResult<ResponseModel>> DeleteEwalletAsync(int idEwallet)
        {
            var isSuccess = await this._serviceMan.DeleteEwalletAsync(idEwallet);

            if (isSuccess == false)
            {
                return BadRequest(new ResponseModel
                {
                    ResponseMessage = "Id not found"
                });
            }
            return Ok(new ResponseModel
            {
                ResponseMessage = "Success delete data"
            });
        }

        [HttpDelete ("delete2",Name ="deleteEwallet2")]

        public async Task<ActionResult<ResponseModel>> DeleteEwallet2Async([FromBody] EWalletModel value)
        {
            var isSuccess = await this._serviceMan.DeleteEwalletAsync(value.IdEwallet);

            if (isSuccess == false)
            {
                return BadRequest(new ResponseModel
                {
                    ResponseMessage = "Id not found"
                });
            }

            return Ok(new ResponseModel
            {
                ResponseMessage = "Success delete data"
            });
        }


        [HttpGet("filter-data")]
        public async Task<ActionResult<List<EWalletModel>>> GetFilterDataAsync([FromQuery] int pageIndex, int itemPerPage, string filterByName)
        {
            var data = await _serviceMan.GetAsync(pageIndex, itemPerPage, filterByName);

            return Ok(data);
        }

        [HttpGet("total-data")]
        public ActionResult<int> GetTotalData()
        {
            var totalData = _serviceMan.GetTotalData();

            return Ok(totalData);
        }
    }
}